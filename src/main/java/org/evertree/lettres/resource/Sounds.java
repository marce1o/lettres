package org.evertree.lettres.resource;

import java.io.BufferedInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sounds {

	public static Clip disappearClip;
	public static Clip gameoverClip;
	public static Clip levelUpClip;
	public static Clip wordFoundClip;
	public static Clip moveDownClip;

	static {
		try {
			disappearClip = AudioSystem.getClip();
			InputStream audioSrc = ClassLoader.getSystemResourceAsStream("org/evertree/lettres/resource/disappear.wav");
			InputStream bufferedIn = new BufferedInputStream(audioSrc);
			AudioInputStream ais = AudioSystem.getAudioInputStream(bufferedIn);
			disappearClip.open(ais);

			gameoverClip = AudioSystem.getClip();
			InputStream audioSrc2 = ClassLoader.getSystemResourceAsStream("org/evertree/lettres/resource/gameover.wav");
			InputStream bufferedIn2 = new BufferedInputStream(audioSrc2);
			AudioInputStream ais2 = AudioSystem.getAudioInputStream(bufferedIn2);
			gameoverClip.open(ais2);

			levelUpClip = AudioSystem.getClip();
			InputStream audioSrc3 = ClassLoader.getSystemResourceAsStream("org/evertree/lettres/resource/levelup.wav");
			InputStream bufferedIn3 = new BufferedInputStream(audioSrc3);
			AudioInputStream ais3 = AudioSystem.getAudioInputStream(bufferedIn3);
			levelUpClip.open(ais3);

			wordFoundClip = AudioSystem.getClip();
			InputStream audioSrc4 = ClassLoader.getSystemResourceAsStream("org/evertree/lettres/resource/wordfound.wav");
			InputStream bufferedIn4 = new BufferedInputStream(audioSrc4);
			AudioInputStream ais4 = AudioSystem.getAudioInputStream(bufferedIn4);
			wordFoundClip.open(ais4);

			moveDownClip = AudioSystem.getClip();
			InputStream audioSrc5 = ClassLoader.getSystemResourceAsStream("org/evertree/lettres/resource/movedown.wav");
			InputStream bufferedIn5 = new BufferedInputStream(audioSrc5);
			AudioInputStream ais5 = AudioSystem.getAudioInputStream(bufferedIn5);
			moveDownClip.open(ais5);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
